# Commerce Balance

Adds a pseudo-field to the Manage Display tab of the commerce order
  which will display the order balance of the order.

For a full description of the module visit
[project page](https://www.drupal.org/project/commerce_balance).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/commerce_balance).


## Requirements

This module requires the following modules:

- [Commerce](https://www.drupal.org/project/commerce)


## Installation

Install the Commerce Balance module as you would normally install a
contributed Drupal module.

Visit [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules) for further information.


## Configuration

- Install Commerce Balance module as normally.
- In Payment methods we can able to see new plugin "Balance (Pay later)".
- Use plugin for the Balance payment methods.
- Once enabled the option will be available in Order.
- Add the fields in manage display.


## Maintainers

- [Panagiotis Moutsopoulos](https://www.drupal.org/u/vensires)
- [Andrei Mateescu](https://www.drupal.org/u/amateescu)
