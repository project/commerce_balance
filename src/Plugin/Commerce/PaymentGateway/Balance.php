<?php

namespace Drupal\commerce_balance\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\ManualPaymentGatewayInterface;
use Drupal\commerce_price\Price;

/**
 * Provides the 'Balance' payment gateway.
 *
 * Needs to be manual so we can skip creating a commerce_payment entity during
 * the checkout process.
 *
 * @CommercePaymentGateway(
 *   id = "balance",
 *   label = "Balance (Pay later)",
 *   display_label = "Balance",
 *   modes = {
 *     "n/a" = @Translation("N/A"),
 *   },
 *   requires_billing_information = TRUE,
 * )
 */
class Balance extends PaymentGatewayBase implements ManualPaymentGatewayInterface {

  /**
   * {@inheritdoc}
   */
  public function buildPaymentInstructions(PaymentInterface $payment) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function canVoidPayment(PaymentInterface $payment) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
  }

  /**
   * {@inheritdoc}
   */
  public function canRefundPayment(PaymentInterface $payment) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $received = FALSE) {
  }

  /**
   * {@inheritdoc}
   */
  public function receivePayment(PaymentInterface $payment, Price $amount = NULL) {
  }

}
