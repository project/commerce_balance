<?php

namespace Drupal\commerce_balance\Plugin\Field\FieldFormatter;

use Drupal\commerce_price\Plugin\Field\FieldFormatter\PriceDefaultFormatter;
use Drupal\commerce_store\Entity\StoreInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'commerce_balance_user_orders' formatter.
 *
 * @FieldFormatter(
 *   id = "commerce_balance_user_orders",
 *   label = @Translation("User orders balance"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class UserOrdersBalanceFormatter extends PriceDefaultFormatter implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity UUID mapper.
   *
   * @var \Drupal\commerce\EntityUuidMapperInterface
   */
  protected $entityUuidMapper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityUuidMapper = $container->get('commerce.entity_uuid_mapper');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // The store UUIDs.
      'stores' => [],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    // Map the UUIDs back to IDs for the form element.
    $store_ids = $this->entityUuidMapper->mapToIds('commerce_store', $this->getSetting('stores'));

    $elements['#element_validate'][] = [
      get_class($this),
      'validateSettingsForm',
    ];
    $elements['stores'] = [
      '#type' => 'commerce_entity_select',
      '#title' => $this->t('Stores'),
      '#default_value' => $store_ids,
      '#target_type' => 'commerce_store',
      '#hide_single_entity' => TRUE,
      '#multiple' => TRUE,
      '#required' => FALSE,
    ];

    return $elements;
  }

  /**
   * Validates the settings form.
   *
   * @param array $element
   *   The settings form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateSettingsForm(array $element, FormStateInterface $form_state) {
    $value = $form_state->getValue($element['#parents']);
    $value['stores'] = \Drupal::service('commerce.entity_uuid_mapper')->mapFromIds('commerce_store', $value['stores']);
    $form_state->setValue($element['#parents'], $value);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    if ($this->getSetting('stores')) {
      $store_ids = $this->entityUuidMapper->mapToIds('commerce_store', $this->getSetting('stores'));
      $stores = $this->entityTypeManager->getStorage('commerce_store')->loadMultiple($store_ids);
      $store_labels = array_map(function (StoreInterface $store) {
        return $store->label();
      }, $stores);
      $summary[] = $this->t('Stores: @stores.', ['@stores' => implode(', ', $store_labels)]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    if ($stores = $this->getSetting('stores')) {
      // Map the UUIDs back to IDs for the form element.
      $store_ids = $this->entityUuidMapper->mapToIds('commerce_store', $stores);

      $items->filter(function ($item) use ($store_ids) {
        /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
        $order = $item->entity;
        return in_array($order->getStoreId(), $store_ids);
      });
    }

    $options = $this->getFormattingOptions();
    $elements = [];
    foreach ($items as $delta => $item) {
      /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
      $order = $item->entity;
      /** @var \Drupal\commerce_price\Price $balance */
      $balance = $order->getBalance();
      $formatted_balance = $this->currencyFormatter->format($balance->getNumber(), $balance->getCurrencyCode(), $options);
      $elements[$delta] = [
        '#markup' => $this->t('Order <a href=":order_link">#@order_number</a>: @balance', [
          ':order_link' => Url::fromRoute('entity.commerce_order.user_view', [
            'commerce_order' => $order->id(),
            'user' => $order->getCustomerId(),
          ])->toString(),
          '@order_number' => $order->label(),
          '@balance' => $formatted_balance,
        ]),
        '#cache' => [
          'contexts' => [
            'languages:' . LanguageInterface::TYPE_INTERFACE,
            'country',
          ],
        ],
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getName() === 'commerce_balance';
  }

}
