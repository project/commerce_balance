<?php

namespace Drupal\commerce_balance;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\user\UserInterface;

/**
 * Computed field item list for the 'commerce_balance' user field.
 */
class UserOrdersBalanceFieldItemList extends FieldItemList implements EntityReferenceFieldItemListInterface {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    foreach ($this->getUserOrdersWithBalance() as $delta => $value) {
      $this->list[$delta] = $this->createItem($delta, $value);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function referencedEntities() {
    return $this->getUserOrdersWithBalance();
  }

  /**
   * Get the user's orders with a positive balance.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface[]
   *   An array of orders.
   */
  protected function getUserOrdersWithBalance() {
    /** @var \Drupal\user\UserInterface $user */
    $user = $this->getEntity();
    if ($user->isNew() || !$user->id()) {
      return [];
    }

    /** @var \Drupal\commerce_order\Entity\OrderInterface $orders */
    $user_orders = \Drupal::entityTypeManager()->getStorage('commerce_order')->loadByProperties([
      'uid' => $user->id(),
    ]);
    $balance_orders = array_filter($user_orders, function (OrderInterface $order) {
      $balance = $order->getBalance();
      return $balance && $balance->isPositive();
    });

    // This will renumber the keys while preserving the order of elements.
    return array_values($balance_orders);
  }

}
